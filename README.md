# HY Retain
[![Built with <3 By Hello you](https://img.shields.io/badge/Built%20with%20%3C3%20By-Helloyou-green.svg)](http://helloyou.digital/)

HY Retain captures several predefined url variables and stores them untill a FormStack form is submitted.

## Setup
There are two steps to using the HY Retain scripts:
1. Add HY Retain script to website.
2. Add HY Retain fields to FormStack form.

Details on both can be found below.
- - -
### Adding the Script
Adding the script is simple. Download this repo and upload hy-retain.js (or hy-retain.min.js) to the website you would like to use it on. __Note:__ The script must be included on all pages. Once complete, you have two places you can add the script to your site:
#### Site `head` element:
```html
<head>
	...
	<script src="js/hy-retain.min.js"></script>
	...
</head>
```
#### Before `</body>` element:
```html
    ...
    <!--hy-retain-script-->
    <script src="js/hy-retain.min.js"></script>
    <!--hy-retain-script-->
    </body>
</html>
```
If you are adding this to multiple sites, it might be better to host this on one of the sites and include it in the others from there. This will make updates easier to handle. To add the script to the _other_ site simply reference the full url in the script tag like so:
```html
<script src="http://example-site.com/js/hy-retain.min.js"></script>
```
### Adding Fields to FormStack Form
We have added a "_Saved Section_" within FormStack that will add the expected fields for HY Retain to work.

![FormStack Section](http://cdn.helloyou.digital/repo-images/hy-retain/01.png "HY Retain - FormStack Section")

Simply Drag this section into any new or existing form and the fields will be populated on sites you have included the script (see above).