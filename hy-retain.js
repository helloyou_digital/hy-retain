/*-----------------------------------------------------------------------------

HY Retain

version: 1.3
updated: 15/12/2017

-----------------------------------------------------------------------------*/

;(function(retain) {

	// options 
	var opts = {
		overwrite: false,
		expire: true,
		expireDays: 60
	};

	// expected url params
	var urlKeys = [
		'campaign_id',
		'utm_campaign',
		'utm_medium',
		'utm_source',
		'utm_term',
		'utm_content'
	];

	// cookie values we set
	var cookieKeys = [
		'meta_campaign_id',
		'meta_campaign',
		'meta_medium',
		'meta_source',
		'meta_term',
		'meta_content',
		'meta_viewport',
		'meta_ad_count',
		'meta_created'
	];

	// default expiry date
	var defaultExpire = 'Fri, 31 Dec 9999 23:59:59 GMT';

	// url utils
	var url = {
		getVar: function(name){
			name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
			var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
			var results = regex.exec(location.search);
			return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
		},
		getVars: function(names){
			var values = {};
			if(typeof names === 'undefined') { return values; }
			for(var i = 0; i < names.length; i++) { 
				values[names[i]] = this.getVar(names[i]);
			}
			return values;
		}
	};

	// cookie utils
	var cookie = {
		getVar: function (sKey) {
			if (!sKey) { return ''; }
			return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || '';
		},
		getVars:function (sKeys) {
			var values = {};
			if(typeof sKeys === 'undefined') { return values; }
			for(var i = 0; i < sKeys.length; i++) { 
				values[sKeys[i]] = this.getVar(sKeys[i]);
			}
			return values;
		},
		setVar: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
			if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
			var sExpires = "";
			if (vEnd) {
				switch (vEnd.constructor) {
					case Number:
						break;
					case String:
						sExpires = "; expires=" + vEnd;
						break;
					case Date:
						sExpires = "; expires=" + vEnd.toUTCString();
						break;
				}
			}
			document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
			return true;
		},
		removeVar: function (sKey, sPath, sDomain) {
			if (!this.hasVar(sKey)) { return false; }
			document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
			return true;
		},
		hasVar: function (sKey) {
			if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
			return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
		},
		keys: function () {
			var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
			for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
			return aKeys;
		}
	};

	// returns string based on window width
	var getWidthName = function() {
		var width = window.innerWidth;
		if(width <= 767){
			return 'MOBILE';
		} else if(width > 767 && width <= 1279){
			return 'TABLET';
		} else {
			return 'DESKTOP';
		}
	};

	// returns current time in YYYYMMDD HH:MM:SS format
	var getNowString = function() {
		var d = new Date();
		var str = d.getUTCFullYear();
		str += ('0' + (d.getUTCMonth() + 1)).slice(-2);
		str += ('0' + d.getUTCDate()).slice(-2);
		str += ' ';
		str += ('0' + d.getUTCHours()).slice(-2) + ':';
		str += ('0' + d.getUTCMinutes()).slice(-2) + ':';
		str += ('0' + d.getUTCSeconds()).slice(-2);
		return str;
	}

	// init script
	retain.init = function() {

		// set values based on current state
		if((this.isUrl() && !this.isCookie()) || (this.isUrl() && opts.overwrite)){
			this.setValues();
		}

		// ad count needs updating regardless of overwrite or expiry
		if(this.isUrl()) {
			this.setAdCount();
		}

		// wait till dom is ready and fill any forms
		if(document.readyState === "complete" || (document.readyState !== "loading" && !document.documentElement.doScroll)){
			this.fillFsForms();
		} else {
			document.addEventListener("DOMContentLoaded", function(){
				retain.fillFsForms();

			});
		}
	}

	// is there url params in the current url
	retain.isUrl = function(){
		var values = url.getVars(urlKeys);
		var found = false;

		for(var key in values){
			if(values[key] !== ''){ found = true; }
		}

		return found;
	};

	// has a cookie with values been set
	retain.isCookie = function(){
		return cookie.hasVar('meta_set');
	};

	// are there any FS forms to fill in
	retain.isForm = function(){
		var re = /formstack\.com/i;
		var forms = document.querySelectorAll('form');
		var found = false;

		if(forms.length === 0){ return false; }

		forms.forEach(function(form){
			if(re.test(form.action)){ found = true;	}
		});

		return found;
	};

	// get object with current values
	retain.getValues = function(){
		return cookie.getVars(cookieKeys);
	};

	// set values based on url params set
	retain.setValues = function(){
		var urlValues = url.getVars(urlKeys);
		var expire = new Date(defaultExpire);
		var values = {
			meta_campaign_id: urlValues.campaign_id,
			meta_campaign: 		urlValues.utm_campaign,
			meta_medium: 			urlValues.utm_medium,
			meta_source: 			urlValues.utm_source,
			meta_term: 				urlValues.utm_term,
			meta_content: 		urlValues.utm_content,

			meta_viewport: 		getWidthName(),
			meta_created: 		getNowString()
		};

		if(opts.expire){
			expire = new Date();
			expire.setTime(expire.getTime()+(opts.expireDays*24*60*60*1000));
		}

		for(var key in values){
			cookie.setVar(key, values[key], expire, '/');
		}

		cookie.setVar('meta_set', '1', expire, '/');
	};

	// increment add count
	retain.setAdCount = function(){
		var expire = new Date(defaultExpire);
		var count = (cookie.hasVar('meta_ad_count')) ? cookie.getVar('meta_ad_count') : 0;
		count = parseInt(count) + 1;
		cookie.setVar('meta_ad_count', count, expire, '/');
	}

	// clear all set values
	retain.clearValues = function(){
		for(var i = 0; i < cookieKeys.length; i++){
			cookie.removeVar(cookieKeys[i]);
			cookie.removeVar('meta_set');
		}
	};

	// fill in FS form
	retain.fillFsForms = function() {
		var re = /formstack\.com/i;
		var values = this.getValues();
		var forms = document.querySelectorAll('form');

		if(! this.isForm()) { return; }

		forms.forEach(function(form){
			if(re.test(form.action)){

				// add values to form
				for(var key in values){
					var input = form.querySelector('[placeholder="'+key+'"]');

					if(input !== null){
						input.value = values[key];
					}
				}

				// clear values on submit
				form.addEventListener('submit', function(e){
					e.preventDefault();
					retain.clearValues();
					e.target.submit();
				});
			}
		});
	};

	retain.init();

})(window.HYRETAIN = {});